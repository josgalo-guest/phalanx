# Portuguese translation for phalanx debconf messages
# Copyright (C) 2007 Luís Picciochi
# This file is distributed under the same license as the phalanx package
# Luís Picciochi, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: phalanx 22+d051004-4\n"
"Report-Msgid-Bugs-To: bartm@knars.be\n"
"POT-Creation-Date: 2006-08-15 00:19+0200\n"
"PO-Revision-Date: 2007-04-11 01:33+0000\n"
"Last-Translator: Luís Picciochi <Pitxyoki@Gmail.com>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../templates:1001
msgid "Learning file size (1-999999):"
msgstr "Tamanho do ficheiro de aprendizagem (1-999999):"

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"The learning file may improve Phalanx strength. Each item in this file uses "
"8 bytes."
msgstr ""
"O ficheiro de aprendizagem pode melhorar a força do Phalanx. Cada item neste "
"ficheiro usa 8 bytes."

#. Type: string
#. Description
#: ../templates:1001
msgid ""
"Please specify a number in the range 1-999999. The size may be changed later "
"by reconfiguring the package but this will erase any data the file may then "
"contain."
msgstr ""
"Por favor especifique um número no intervalo 1-999999. O tamanho pode ser "
"alterado mais tarde ao reconfigurar o pacote mas isso irá apagar quaisquer "
"dados que o ficheiro possa conter na altura."

#. Type: boolean
#. Description
#: ../templates:2001
msgid "Really change the size of the learning file and erase its data?"
msgstr "De certeza que quer alterar o tamanho do ficheiro de aprendizagem e "
"apagar os seus dados?"

#. Type: boolean
#. Description
#: ../templates:2001
msgid ""
"You chose to change the size of the learning file used by phalanx for "
"improving its strength.  This will erase all the learned data."
msgstr ""
"Escolheu alterar o tamanho do ficheiro de aprendizagem usado pelo phalanx "
"para melhorar a sua força. Isto irá apagar todos os dados aprendidos."
